﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoTest
{
    class RadixSort
    {

        public List<int> Items { get; set; } = new List<int>();
     
         

        public RadixSort(List<int> items)
        {
            Items.AddRange(items);
        }
        public void MakeSort()
        {

            var groups = new List<List<int>>();

            for (int i = 0; i < 10; i++)
            {
                groups.Add(new List<int>());
            }

            int lenght =  GetMaxLenght();
           
            for(int step = 0;step<lenght;step++)
            {
                //Розподіл по корзинам
                foreach (int item in Items)
                {
                    var i = item.GetHashCode();
                    var value = i % (int)Math.Pow(10,step + 1) / (int)Math.Pow(10,step); // 387%1000/100 = 3
                    groups[value].Add(item);                                                            
                }
                Items.Clear();
                //Збір елементів
                foreach (var group in groups)
                {
                    foreach (var item in group)
                    {
                        Items.Add(item);
                    }
                }
                //Чищення корзин
                foreach(var group in groups)
                {
                    group.Clear();
                }
            }
        }
        private int GetMaxLenght()
        {
            int lenght = 0;
            foreach (int item in Items)
            {
                if (item.GetHashCode() < 0)
                {
                    throw new ArgumentException("Only integer > 0");
                }
                //int l = Convert.ToInt32(Math.Log10(item.GetHashCode()) + 1);
                var l = item.GetHashCode().ToString().Length;
                if (l > lenght)
                {
                    lenght = l;
                }
            }
            return lenght;
        }
    }
}
