﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoTest
{
    class Program
    {
       
        static void Main(string[] args)
        {
            Random rnd = new Random();
            List<int> Items = new List<int>();
            int N = 100000, X=1000, t;
            Stopwatch stopwatch = new Stopwatch();
            
            Items.Clear();
            for (int i = 0; i < N; i++)
            {
                Items.Add(rnd.Next(0, X));
            }
           // Console.WriteLine("Випадково згенерована послідовність");
           //Out(Items);
            //Console.WriteLine();
            //Console.WriteLine("Результат:");
            stopwatch.Start();
            Items = RadixSortTest(Items);
            stopwatch.Stop();

            Console.WriteLine($"N = {N}\nX = {X}\nЧас виконання: {stopwatch.ElapsedMilliseconds} мс");

            //Out(Items);
            Console.ReadLine();

        }

        public static List<int> RadixSortTest(List<int> items)
        {
            var radix = new RadixSort(items); 
            radix.MakeSort();
            return radix.Items;
           

        }
        public static void Out(List<int> items)
        {
            foreach (var item in items)
            {
                Console.Write(item + " ");
            }
        }
    }
}
